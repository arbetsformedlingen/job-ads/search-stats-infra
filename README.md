# Search-trends-infra

## Configuration for running search-trends in Openshift

## Code repo

https://gitlab.com/arbetsformedlingen/job-ads/search-trends
Read the README and other documentation (in /docs) to understand what the program does and what variables needs to be
and secrets needs to be configured. For this repo, the file `docs/pipeline.md` is most relevant.
