apiVersion: tekton.dev/v1
kind: Pipeline
metadata:
  annotations:
    ignore-check.kube-linter.io/latest-tag: "Ignore since it is a batch job and we want security updates"
  name: search-trends-pipeline
spec:
  workspaces:
    - name: shared-workspace
  params:
    - name: date
      description: Date to extract
      default: ""
    - name: public-bucket
      description: Destination bucket for processed files on S3
      default: data.jobtechdev.se-adhoc-test
    - name: private-bucket
      description: Destination bucket for private processed files on Minio
      default: search-trends-dev
  tasks:
    - name: get-date
      params:
        - name: date
          value: "$(params.date)"
      taskSpec:
        params:
          - name: date
            type: string
        results:
          - name: date
            description: Date to process. Same as parameter if not empty, then previous date.
        steps:
          - name: get-date
            image: docker-images.jobtechdev.se/script-container/script-container:latest
            imagePullPolicy: Always
            script: |
              if [ -z "$(params.date)" ] ; then
                date --date="1 day ago"  +%Y-%m-%d | tr -d "\n"> $(results.date.path)
              else
                echo -n $(params.date) > $(results.date.path)
              fi
    - name: process
      workspaces:
        - name: work
          workspace: shared-workspace
      params:
        - name: date #Not used right now.
          value: $(tasks.get-date.results.date)
      taskSpec:
        params:
          - name: date
            type: string
        workspaces:
          - name: work
            description: Where to read and write files from.
        steps:
          - name: process
            image: docker-images.jobtechdev.se/search-trends/search-trends:latest
            workingDir: $(workspaces.work.path)
            env:
              - name: WORK_DIR
                value: $(workspaces.work.path)
              - name: DATE
                value: $(params.date)
            envFrom:
              - secretRef:
                  name: elastic
              - configMapRef:
                  name: search-trends-pipeline
            command:
              - collect_stats
      runAfter:
        - get-date
    - name: zip-public
      workspaces:
        - name: work
          workspace: shared-workspace
      params:
        - name: source
          value: "*public.json"
        - name: destination
          value: jobsearch-daily-$(tasks.get-date.results.date).zip
      taskRef:
        name: zip
        kind: Task
      runAfter:
        - process
    - name: write-s3-aws-public
      params:
        - name: bucket
          value: "$(params.public-bucket)"
        - name: destination
          value: /annonser/search-trends/jobsearch-daily-$(tasks.get-date.results.date).zip
        - name: source
          value: $(workspaces.work.path)/jobsearch-daily-$(tasks.get-date.results.date).zip
        - name: aws-secret
          value: file-publish-s3
      workspaces:
        - name: work
          workspace: shared-workspace
      taskRef:
        name: s3-upload
        kind: Task
      runAfter:
        - zip-public
    - name: zip-internal
      workspaces:
        - name: work
          workspace: shared-workspace
      params:
        - name: source
          value: "*INTERNAL.json"
        - name: destination
          value: jobsearch-daily-internal-$(tasks.get-date.results.date).zip
      taskRef:
        name: zip
        kind: Task
      runAfter:
        - process
    - name: write-daily-internal-minio
      params:
        - name: source
          value: $(workspaces.work.path)/jobsearch-daily-internal-$(tasks.get-date.results.date).zip
        - name: destination
          value: /stats/jobsearch-daily-$(tasks.get-date.results.date).zip
        - name: bucket
          value: $(params.private-bucket)
        - name: mc-host-secret-ref
          value: minio-secret
      workspaces:
        - name: work
          workspace: shared-workspace
      taskRef:
        name: minio-upload
        kind: Task
      runAfter:
        - zip-internal
    - name: zip-raw
      workspaces:
        - name: work
          workspace: shared-workspace
      params:
        - name: source
          value: "$(tasks.get-date.results.date)/*"
        - name: destination
          value: $(tasks.get-date.results.date)-raw.zip
      taskRef:
        name: zip
        kind: Task
      runAfter:
        - process
    - name: write-raw-minio
      params:
        - name: source
          value: $(workspaces.work.path)/$(tasks.get-date.results.date)-raw.zip
        - name: destination
          value: /stats/raw/$(tasks.get-date.results.date)-raw.zip
        - name: bucket
          value: $(params.private-bucket)
        - name: mc-host-secret-ref
          value: minio-secret
      workspaces:
        - name: work
          workspace: shared-workspace
      taskRef:
        name: minio-upload
        kind: Task
      runAfter:
        - zip-raw
  finally:
    - name: inspect
      # This task is only here to help inspection of files when troubleshooting. Remove when fully live.
      workspaces:
        - name: work
          workspace: shared-workspace
      taskSpec:
        workspaces:
          - name: work
            description: Where to read and write files from.
        steps:
          - name: list
            image: docker-images.jobtechdev.se/script-container/script-container:latest
            imagePullPolicy: Always
            workingDir: $(workspaces.work.path)
            script: |
              ls -Ral
